const express = require('express');
const router = express.Router();
const Post = require('../models/postm');
//แสดงข้อมูลทั้งหมดของ collection posts
router.get('/', async (req, res) => {
     //แสดงข้อความเมือมีการ เรียก get
     // res.send('We are on posts');
     try{
          const posts = await Post.find();
          res.json(posts);
     }catch(err){
          res.json({message:err});
     }
});

router.get('/specific',(req, res) => {
     msg = {
          "status":"success"  ,
          "aaaaaa":"bbbbbbb" ,
          "dfdfdf":"bbdfdfdfdbbbbb" ,
          "data":  "lalalalal"       
 }
     res.json(msg);
 });

//แสดงข้อมูลเเบบ ส่งค่า ID  
//แบบเเรก
/*
router.get('/:postId',(req, res) => {
     
     console.log(req.params.postId);
    
}); */

//แสดงข้อมูลเเบบ ส่งค่า ID 
//แบบสอง
router.get('/:postId', async (req, res) => {
     
     try {
          const postidss = await Post.findById(req.params.postId);
          res.json(postidss);
     } catch (err) {
          res.json({message: err});
     }
});






// เเบบ ที่หนึ่ง สามารถ insert ได้เหมือนกัน
/*router.post('/' , (req, res) => {
   // console.log(req.body);
   const posts = new Post ({
        title: req.body.title,
        description: req.body.description
   });

   posts.save()
   .then(data => {
        res.json(data);
   })
   .catch(err =>{
        res.json({message: err});
   });
});*/

// เเบบ ที่สอง สามารถ insert ได้เหมือนกัน
router.post('/' , async (req, res) => {
   // console.log(req.body);
   const posts = new Post ({
        title: req.body.title,
        description: req.body.description
   });

  try{
        const savedPosts = await posts.save();
        lamdevmsg = {
                 "status":"success"  ,
                 "aaaaaa":"bbbbbbb" ,
                 "dfdfdf":"bbdfdfdfdbbbbb" ,
                 "data":  savedPosts       
        }
        res.json(lamdevmsg);
  }catch{
      res.json({message: err});
  }
});

//Delate mathod
router.delete('/:postIdd', async (req, res) => {
     try {
          const removedPost = await Post.remove({ _id: req.params.postIdd});
          console.log(req.params.postIdd);
          res.json(removedPost);
     } catch (err) {
          res.json({message: err});
     }
   
});
// Update Mathod
router.patch('/:postIdUpdate', async (req, res) => {
     try {
          const updatePost = await Post.updateOne(
               {_id: req.params.postIdUpdate},
               { $set: { title: req.body.title }}
               );
          res.json(updatePost);
     } catch (err) {
          res.json({message:err});
     }
});
module.exports = router;

const express = require('express');

const lamapp = express();
const mongoose = require('mongoose');
const bodyParser =require('body-parser');
require('dotenv/config')

lamapp.use(bodyParser.json());
//Middlewares
//lamapp.use('/post',() => {
//    console.log('LAm test Middlewares')
//});

//import Router
const postRoute = require('./router/post');

lamapp.use('/post',postRoute);


//routes
lamapp.get('/',(req,res) =>{
    res.send('welcome To Api ')
})
//lamapp.get('/post',(req,res) =>{
//    res.send('we are on posts ')
//})

//Connect to DB 
mongoose.connect(process.env.DB_CONNECTION,{ useNewUrlParser: true },() => 
console.log('Connect to DB!')
);

//how to we start lsitrning to the server 
lamapp.listen(3000);

